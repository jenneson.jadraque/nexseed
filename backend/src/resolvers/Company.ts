import { Company } from '../entity/Company';
import { Department } from '../entity/Department';
import { User } from '../entity/User';
import {Resolver, Query, Mutation, Arg, Ctx, UseMiddleware, Int} from 'type-graphql';
import { UserContext } from '../context/UserContext';
import { isAuth } from '../middleware/isAuth';

@Resolver()
export class CompanyResolver{
    @Mutation(() => Boolean)
    @UseMiddleware(isAuth)
    async addCompanyAndToUser(
        @Arg('company_id') companyId: number,
        @Ctx() {payload}: UserContext
    ) {
        const userId = payload!.userId
        const user = await User.findOne(
            {
                where: {id: userId}
            }
        );

        if(user){
            const company = await Company.findOne(
                {
                    where: {id: companyId}
                }
            );
            
            if(company){
                user.company = company;
                await User.save(user);
            }
        }

        return true;
    }

    async addDepartmentAndToUser(
        @Arg('department_id') departmentId: number,
        @Ctx() {payload}: UserContext
    ) {
        const userId = payload!.userId
        const user = await User.findOne(
            {
                where: {id: userId}
            }
        );

        if(user){
            const department = await Department.findOne(
                {
                    where: {id: departmentId}
                }
            );
            
            if(department){
                department.users.push(user);
                await Department.save(department);
            }
        }

        return true;
    }

    @Mutation(() => Boolean)
    @UseMiddleware(isAuth)
    async saveCompany(
        @Arg('company_name') companyName: string,
        @Ctx() {payload}: UserContext
    ) {
        
        await Company.insert(
            {
                name: companyName
            }
        )
 
        return true;
    }


}