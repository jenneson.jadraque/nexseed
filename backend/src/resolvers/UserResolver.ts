import {Resolver, Query, Mutation, Arg, Ctx, UseMiddleware, Int} from 'type-graphql';
import {hash, compare} from 'bcryptjs';
import { User } from '../entity/User';
import { UserContext } from '../context/UserContext';
import { createAccessToken } from '../lib/auth';
import { isAuth } from '../middleware/isAuth';
import { sendRefreshToken } from '../lib/sendRefreshToken';
import {LoginResponse} from '../responses/LoginResponse';


@Resolver()
export class UserResolver{
    @Query(() => [User])
    @UseMiddleware(isAuth)
    users(){
        return User.find();
    }

    @Mutation(() => Boolean)
    async logout(@Ctx() { res }: UserContext) {
        sendRefreshToken(res, "");
        return true;
    }

    @Mutation(() => String)
    async register(
        @Arg('username') username: string,
        @Arg('password') password: string
    ){      
        let user:any = await User.findOne(
            {
                where: {username: username}
            }
        );

        if(user){
            throw new Error ('User already exists.');
        }else{
            const hashedPassword = await hash(password,  12);

            await User.insert(
                {
                    username: username,
                    password: hashedPassword
                }
            )
     
           return "success";
        }
    }
    
    @Mutation(() => LoginResponse)
    async login(
        @Arg('username') username: string,
        @Arg('password') password: string,
        @Ctx() {res}: UserContext
    ): Promise<LoginResponse> {
        
        const user = await User.findOne({where: [
            {username:username}
        ]});

        if(!user || (user && !user.password)){
            throw new Error ('Invalid username or password');
        }

        const valid = await compare(password, user.password);

        if(!valid){
            throw new Error ('Invalid password');
        }

        return {
            accessToken: await createAccessToken(user)
        }; 
    }

}