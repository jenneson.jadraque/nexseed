import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToMany, JoinTable, OneToMany} from "typeorm";
import { ObjectType, Field, Int} from "type-graphql";
import { Department } from "./Department";


@ObjectType()
@Entity("companies")
export class Company extends BaseEntity{
    @Field(() => Int)
    @PrimaryGeneratedColumn()
    id: number;

    @Field({ nullable: true })
    @Column("text")
    name: string;

    @OneToMany(() => Department, department => department.company)
    departments: Department[];
}