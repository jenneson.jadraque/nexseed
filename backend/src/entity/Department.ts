import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, ManyToMany, JoinTable} from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { User } from "./User";
import { Company } from "./Company";


@ObjectType()
@Entity("departments")
export class Department extends BaseEntity{
    @Field(() => Int)
    @PrimaryGeneratedColumn()
    id: number; 

    @Field({ nullable: true })
    @Column("text")
    name: string; 

    @ManyToOne(() => Company, company => company.departments)
    @JoinTable()
    company: Company;
    
    @ManyToMany(() => User)
    @JoinTable()
    users: User[];
}