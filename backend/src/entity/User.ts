import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, ManyToMany, JoinTable, OneToOne, JoinColumn} from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { Department } from "./Department";
import { Company } from "./Company";


@ObjectType()
@Entity("users")
export class User extends BaseEntity{
    @Field(() => Int)
    @PrimaryGeneratedColumn()
    id: number;

    @Field({ nullable: true })
    @Column("text")
    username: string;

    @Column("text", {default: null})
    password: string;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    time: string;

    @Field({ nullable: true })
    @OneToOne(() => Company)
    @JoinColumn()
    company: Company;
}