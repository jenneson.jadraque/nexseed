import 'dotenv/config';
import "reflect-metadata";
import express from "express";
import {ApolloServer} from "apollo-server-express";
import {buildSchema} from "type-graphql";
import {createConnection} from "typeorm";
import cookieParser from "cookie-parser";
import path from 'path';
import cors  from 'cors';
import http from 'http';
import {dbOptionsDevelopLocal} from './typeormconfig'
import { UserResolver } from "./resolvers/UserResolver";
import { CompanyResolver } from './resolvers/Company';

(async() => {
    let urlFrontEnd:any = process.env.DEV_URL_FRONTEND;
    let usePort:any = process.env.DEVELOPMENT_PORT;
    let dbOptions: any = dbOptionsDevelopLocal;
    let httpVal:any = http;

    //root directory
    process.env.NODE_PATH = path.resolve(__dirname);
    process.env.FRONT_END_URL = urlFrontEnd;

    const app = express();
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, "public")));
    
    //To allow cross-origin requests
    app.use(cors(
      {
        origin: urlFrontEnd,
        credentials: true
      }
    ));

    await createConnection(dbOptions).catch((error) =>
      console.log("Error found while trying to connect to mysql : ", error)
    );
    
    const apolloserver = new ApolloServer({
        schema: await buildSchema({
            resolvers: [
              UserResolver,
              CompanyResolver
            ]
        }),
        context: ({req, res}) => ({req, res}),
        introspection: true
    });
 
    apolloserver.applyMiddleware({app, cors: false});

    app.set('port', usePort);

    const server = httpVal.createServer(app);
    server.listen(usePort);

    server.on('error', onError);
    server.on('listening', onListening);

    function onError(error: any) {
        if (error.syscall !== 'listen') {
          throw error;
        }
      
        var bind = typeof usePort === 'string'
          ? 'Pipe ' + usePort
          : 'Port ' + usePort;
      
        // handle specific listen errors with friendly messages
        switch (error.code) {
          case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
          case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
          default:
            throw error;
        }
      }
      
      function onListening() {
        console.log("running on port at http "+usePort+"....");
        console.log("running on port at ws "+usePort+"....");
      }
})();