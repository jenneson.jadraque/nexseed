import { User } from "../entity/User";
import { sign } from 'jsonwebtoken';

export const createAccessToken = async (user: User) => {
    return sign(
        {
            userId: user.id
          
        }, 
        process.env.ACCESS_TOKEN_SECRET!
    ); 
}