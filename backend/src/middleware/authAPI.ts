import { verify } from 'jsonwebtoken';
import {notFoundResponse, unauthorizedResponse} from '../helper/ApiResponse';

export const authenticate = function (req:any, res:any, next:any) {

	const authHeader = req.headers['authorization'];
	const token  = authHeader && authHeader.split(' ')[1];
	if(token == null) return unauthorizedResponse(res, "Authentication failed");

	verify(token, process.env.ACCESS_TOKEN_SECRET!, (err:any, user:any) => {
		if(err){
            const token = req.cookies.mcokie;
            if(!token){
                return unauthorizedResponse(res, "Authentication failed");
            }
            verify(token, process.env.REFRESH_TOKEN_SECRET!, (e:any, u:any) => {
                if(e)
                    return notFoundResponse(res, "Invalid token");
                req.user = u;
                next()
            });    
        }else{
            req.user = user;
            next()
        }
	});
};