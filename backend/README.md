
# Example Project Build with TypeORM

Steps to run this project:

1. Run `npm install` command
2. Setup database settings inside `src/typeormconfig.ts` file
3. Run `npm startdev` or `npm start` command

### using graphql playground

>  http://localhost:4500/graphql

Please see the documentation in playground

- Example mutation call
`mutation{
  register(username: "1231231", password: "123123")
}`


